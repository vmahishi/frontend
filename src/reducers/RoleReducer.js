import {ROLE} from '../actions/actionType'

const RoleReducer = (state=[], action)=> {
    switch(action.type){
        case ROLE:
            return action.role
        default:
            return state
    }
}

export default RoleReducer
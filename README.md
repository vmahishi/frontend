# README #

### What is this repository for? ###

* Front end built with reactjs and material UI for job search and application portal

### How do I get set up? ###

* Summary of set up

*    Open a terminal on your system
*    Enter git clone git clone git clone https://vmahishi@bitbucket.org/vmahishi/frontend.git  and hit enter
*    Change your directory to frontend
*    On the terminal enter 'yarn install' or 'npm install' based on the package manager installed
*    Once the installation is complete, enter 'yarn start' or 'npm start' and hit enter
*    Ta da, your front end is up and running
*   The frontend opens up at localhost:3000
*   Home page is the job listing for anybody looking for a job, people can check out the details and continue to apply from there.
*   The sign in option is only for the admin of the portal for them to add new jobs, edit job details, view applications etc.
*   Enter any character in the username field of the sign in page and click on sign in. I couldn't do any validations for this. Sorry
### Once logged in,
*   Dashboard : This is to check out latest applications, a graph of incoming applications etc.
*   Add Job : This is for the admin of the job portal to enter new job postings
*   View Jobs : This is again for the admin to have a look about the open job positions, from here you can edit a job posting or delete it if it has become redundant.
*   Applications : This tab is to keep an eye on the latest applications received for different companies and for different job roles.
*   Reports and Integrations are just dummy tabs, they are not functional, but have been added to get a feel of how this looks.
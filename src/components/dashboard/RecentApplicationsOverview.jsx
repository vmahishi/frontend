import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Title from './Title';

const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});

export default function RecentApplicationsOverview() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Recent Applications</Title>
        <Typography component="p" variant="h4">
          1,234
        </Typography>
        <Typography color="textSecondary" className={classes.depositContext}>
          {new Date().toLocaleDateString()}
        </Typography>
    </React.Fragment>
  );
}

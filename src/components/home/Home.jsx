import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from './home.css';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import {ROLE, JOB_ID, COMPANY_NAME} from '../../actions/actionType';
import WireInfo from './JobDetails'

export default class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            showComponent : false,
            id: '',
            showModal : false,
            body : {}
        }
        this._onButtonClick = this._onButtonClick.bind(this);
        this._onButtonClickApply = this._onButtonClickApply.bind(this);
    }

    componentDidMount = () => {
        const { dispatch, actions } = this.props
        dispatch(actions.getAllJobs())
    }

     _onButtonClick() {
        let path = '/signin';
        this.props.history.push(path);
    }

      handleClose = () => {
        this.setState({
            showModal : false
        })
        const path ='./apply'
        this.props.history.push(path);
    };

    _onButtonClickApply(body, jobId, company){
        const { dispatch } = this.props
        dispatch(
            {
                type: ROLE,                
                role: body.role
            }
        )
        dispatch(
            {
                type: COMPANY_NAME,                
                companyName: company
            }
        )
        dispatch(
            {
                type: JOB_ID,                
                jobId: jobId
            }
        )
         this.setState({
            showModal : true,
            id : jobId,
            body : body
        })
    }

    render(){
        const jobs = this.props.jobList
            return (
                <div>
                    <AppBar position="static">
                        <Toolbar>
                            <Grid
                                justify="space-between"
                                container 
                                spacing={24}
                            >
                                <Grid item>
                                    <Typography type="title" color="inherit">
                                        Good Worker
                                    </Typography>
                                </Grid>
                                <Grid item>
                                    <div>
                                        <Button color="inherit" onClick={this._onButtonClick}>Login</Button>
                                    </div>
                                </Grid>
                            </Grid>
                        </Toolbar>
                        </AppBar>
                    <TableContainer component={Paper}>
                        <Table className={styles.table} aria-label="simple table">
                            <TableHead>
                            <TableRow>
                                <TableCell>Company</TableCell>
                                <TableCell align="left">Role</TableCell>
                                <TableCell align="left">Experience</TableCell>
                                <TableCell align="left">Skills</TableCell>
                                <TableCell align="left">Key Responsibilities</TableCell>
                                <TableCell align="right" size='small'>Action</TableCell>
                            </TableRow>
                            </TableHead>
                            <TableBody>
                            {jobs && jobs.map((job) => (
                                <TableRow key={job._id}>
                                <TableCell component="th" scope="row">
                                    {JSON.parse(job.body).company}
                                </TableCell>
                                <TableCell align="left">{JSON.parse(job.body).role}</TableCell>
                                <TableCell align="left">{JSON.parse(job.body).exp}</TableCell>
                                <TableCell align="left">{JSON.parse(job.body).skills.substring(0, 40) + '...'}</TableCell>
                                <TableCell align="left">{JSON.parse(job.body).responsibility.substring(0, 100) + '...'}</TableCell>
                                <TableCell align="right" size='small'>
                                    <Button variant="contained" color="primary" onClick={() => this._onButtonClickApply(JSON.parse(job.body), job._id, JSON.parse(job.body).company)}>
                                        Details
                                    </Button>
                                </TableCell>
                                </TableRow>
                            ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    {
                        this.state.showModal &&
                        <WireInfo handleClose={this.handleClose} id={this.state.id} body={this.state.body} actions={this.props.actions} dispatch={this.props.dispatch}/>
                    }
                </div>
            )
        }
}
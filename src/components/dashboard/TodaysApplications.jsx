import React from 'react';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from './Title';

// Generate Order Data
function createData(id, date, name, company, role, experience, match) {
  return { id, date, name, company, role, experience, match};
}

const rows = [
  createData(0, new Date().toLocaleDateString(), 'Elvis Presley', 'Google Inc', 'Product Designer', '8 years', '80%'),
  createData(1, new Date().toLocaleDateString(), 'Paul McCartney', 'Trustpilot', 'Project Manager', '12 years', '34%'),
  createData(2, new Date().toLocaleDateString(), 'Tom Scholz', 'Good Worker', 'Director', '22 years', '68%'),
  createData(3, new Date().toLocaleDateString(), 'Michael Jackson', 'Taantrix Inc', 'Engineering Manager', '15 years', '98%'),
  createData(4, new Date().toLocaleDateString(), 'Bruce Springsteen', 'Swiggy', 'Associate Engineer', '2 years', '8%'),
];

function preventDefault(event) {
  event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
  seeMore: {
    marginTop: theme.spacing(3),
  },
}));

export default function Orders() {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Today's Applications</Title>
      <Table size="small">
        <TableHead>
          <TableRow>
            <TableCell>Date</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Company</TableCell>
            <TableCell>Job Role</TableCell>
            <TableCell align="right">Experience</TableCell>
            <TableCell align="right">Match %</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.id}>
              <TableCell>{row.date}</TableCell>
              <TableCell>{row.name}</TableCell>
              <TableCell>{row.company}</TableCell>
              <TableCell>{row.role}</TableCell>
              <TableCell align="right">{row.experience}</TableCell>
              <TableCell align="right">{row.match}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div className={classes.seeMore}>
        <Link color="primary" href="#" onClick={preventDefault}>
          See more applications
        </Link>
      </div>
    </React.Fragment>
  );
}

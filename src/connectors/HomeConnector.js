import {connect} from 'react-redux'
import {getAllJobs} from '../actions/JobAction'
import Home from '../components/home/Home'

function mapStateToProps(state){
  return{
    jobList: state.jobList,
    actions:{
        getAllJobs:getAllJobs
    }
  } 
}
export default connect(mapStateToProps)(Home)

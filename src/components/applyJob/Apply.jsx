import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import CheckIcon from '@material-ui/icons/Check';
import { Alert, AlertTitle } from '@material-ui/lab';
import './Apply.css'

export default class Apply extends Component {

  constructor(props){
      super(props)
      this.state = {
          firstName : '',
          lastName : '',
          email : '',
          mobile : '',
          exp:'',
          currentCTC : '',
          expectedCTC : '',
          noticePeriod : '',
          skills : '',
          resume : '',
          open : false
      }
      this.readInput = this.readInput.bind(this)
      this.applyJob = this.applyJob.bind(this)
  }

  readInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    applyJob = () => {
        const body = {
            jobId : this.props.jobId,
            company : this.props.companyName,
            role : this.props.role,
            firstName : this.state.firstName,
            lastName : this.state.lastName,
            exp: this.state.exp,
            email : this.state.email,
            mobile : this.state.mobile,
            currentCTC :  this.state.currentCTC,
            expectedCTC : this.state.expectedCTC,
            noticePeriod : this.state.noticePeriod,
            skills : this.state.skills,
            resume :  this.state.resume
        }
        const {actions} = this.props
        let jobResponse = (actions.applyJob(body))
        jobResponse.then((status) => {
            if(status === 200){
                this.setState({
                    open : true
                })
                setTimeout(() => { 
                    this.setState(() => ({open : false}))
                    let path = '/';
                    this.props.history.push(path);
                }, 5000);
            }
            else{

            }
        })
    }

  render(){
      return (
          <div>
            <AppBar position="static">
                <Toolbar>
                    <Grid
                        justify="space-between"
                        container 
                        spacing={24}
                    >
                        <Grid item>
                            <Typography type="title" color="inherit">
                                Application for the role of {this.props.role}
                            </Typography>
                        </Grid>
                    </Grid>
                </Toolbar>
                </AppBar>
                {
                    this.state.open && 
                    <Snackbar open={this.state.open} autoHideDuration={6000}>
                        <Alert icon={<CheckIcon fontSize="inherit" />} variant="filled" severity="success"> 
                                <AlertTitle>You have successfully applied for the position of {this.props.role}</AlertTitle>
                            Please wait for us to review your application and get in touch with you
                        </Alert>
                    </Snackbar>
                }
                <form className='root' noValidate autoComplete="off">
                    <div>
                        <TextField
                        id="outlined-multiline-flexible"
                        label="First Name"
                        name="firstName"
                        multiline
                        rowsMax={4}
                        value={this.state.firstName}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        name="lastName"
                        label="Last Name"
                        value={this.state.lastName}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                        id="outlined-multiline-flexible"
                        name="email"
                        label="E-mail"
                        value={this.state.email}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Mobile Number"
                        name="mobile"
                        type="number"
                        value={this.state.mobile}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                        id="outlined-multiline-flexible"
                        name="currentCTC"
                        label="Current CTC"
                        value={this.state.currentCTC}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        name="expectedCTC"
                        label="Expected CTC"
                        value={this.state.expectedCTC}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Total Experience"
                        name="exp"
                        value={this.state.exp}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        name="noticePeriod"
                        label="Notice Period"
                        value={this.state.noticePeriod}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                            id="outlined-full-width"
                            name="skills"
                            label="Primary Skills"
                            style={{ margin: 8 }}
                            placeholder="Placeholder"
                            value={this.state.skills}
                            fullWidth
                            onChange={this.readInput}
                            margin="normal"
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                        />
                        <br/>
                        <TextField
                            id="outlined-full-width"
                            name="resume"
                            label="Copy Paste your Resume"
                            style={{ margin: 8 }}
                            placeholder="Placeholder"
                            value={this.state.resume}
                            fullWidth
                            onChange={this.readInput}
                            margin="normal"
                            multiline
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            />
                    </div>
                </form>
                <Button variant="contained" color="primary" onClick={this.applyJob}>
                    Apply
                </Button>
            </div>
  );
  }
}

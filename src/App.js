import {Switch,Route, BrowserRouter} from 'react-router-dom'
import HomeConnector from './connectors/HomeConnector';
import DashboardConnector from './connectors/DashboardConnector';
import SignIn from './components/signin/SignIn';
import ApplyJobConnector from './connectors/ApplyJobConnector';
import './App.css';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route exact path='/' component={HomeConnector} />
          <Route exact path='/signin' component={SignIn} />
          <Route exact path='/dashboard' component={DashboardConnector} />
          <Route exact path='/apply' component={ApplyJobConnector} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;

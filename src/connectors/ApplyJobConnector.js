import {connect} from 'react-redux'
import Apply from '../components/applyJob/Apply'
import {applyJob} from '../actions/JobAction'

function mapStateToProps(state){
  return{
    role: state.role,
    jobId : state.jobId,
    companyName:state.companyName,
    actions:{
      applyJob: applyJob
    }
  } 
}
export default connect(mapStateToProps)(Apply)

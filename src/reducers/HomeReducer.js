import {JOB_LIST} from '../actions/actionType'

const HomeReducer = (state=[], action)=> {
    switch(action.type){
        case JOB_LIST:
            return action.jobList
        default:
            return state
    }
}

export default HomeReducer
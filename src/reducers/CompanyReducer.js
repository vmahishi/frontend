import {COMPANY_NAME} from '../actions/actionType'

const CompanyReducer = (state=[], action)=> {
    switch(action.type){
        case COMPANY_NAME:
            return action.companyName
        default:
            return state
    }
}

export default CompanyReducer
import { combineReducers } from 'redux'
import storage from 'redux-persist/lib/storage'
import jobList from './HomeReducer'
import role from './RoleReducer'
import jobId from './JobIdReducer'
import jobApplications from './ApplicationReducer'
import companyName from './CompanyReducer'


const AppReducer = combineReducers({
  jobList,
  role,
  jobId,
  jobApplications,
  companyName
})

const rootReducer = (state, action) => {
      if (action.type === 'USER_LOGOUT') {
          Object.keys(state).forEach(key => {
            storage.removeItem(`persist:${key}`)
          })
          state = undefined;
      }
      return AppReducer(state, action)
  }
   
export default rootReducer
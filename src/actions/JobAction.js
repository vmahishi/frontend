import { postAPI, getAPI, deleteAPI, putAPI, apiLocation } from '../Util'
import {JOB_LIST, ERROR, JOB_APPLICATIONS} from './actionType'

function receiveJobList(json) {
    return {
        type: JOB_LIST,
        jobList: json,
    }
}

function receiveApplications(json) {
    return {
        type: JOB_APPLICATIONS,
        jobApplications: json,
    }
}

function receiveError(errorCode) {
    return {
        type: ERROR,
        errorCode: errorCode,
    }
}

function client_getAllJobs() {
    var api =  `${apiLocation()}/jobs/`;
    return dispatch => {
        return fetch(api,getAPI())
            .then(response => {
            return response.json()
        })
            .then(json => dispatch(receiveJobList(json)))
    }
}

export function getAllJobs() {
    return dispatch => {
        return dispatch(client_getAllJobs())
    }
}

function client_getAllApplications() {
    var api =  `${apiLocation()}/applications`;
    return dispatch => {
        return fetch(api,getAPI())
            .then(response => {
            return response.json()
        })
            .then(json => dispatch(receiveApplications(json)))
    }
}

export function getAllApplications() {
    return dispatch => {
        return dispatch(client_getAllApplications())
    }
}

function client_applyJob(body) {
    return fetch(`${apiLocation()}/apply`,
    postAPI(body)
    )
    .then(response => {
        return response.status
    })
    .then(status => {
            if(status === 200)
            {
                return status
            }
        })
}
export function applyJob(body) {
    return (client_applyJob(body))
}

function client_addJob(body) {
    return dispatch => {
        return fetch(`${apiLocation()}/add`,
            postAPI(body)
        )
            .then(response => {
                return response.status
            })
            .then(status => {
                if (status === 500) {
                    dispatch((receiveError(status)))
                }
                else {
                    dispatch(client_getAllJobs())
                    dispatch((receiveError(status)))
                }
            })
    }
}
export function addJob(body) {
    return dispatch => {
        return dispatch(client_addJob(body))
    }
}

function client_deleteJob(id) {
    return dispatch => {
        return fetch(`${apiLocation()}/delete/${id}`,
            deleteAPI())
            .then(response => {
                return response
            })
            .then(
                dispatch(client_getAllJobs())
            )
    }
}

export function deleteJob(id) {
    return dispatch => {
        return dispatch(client_deleteJob(id))
    }
}

function client_updateJob(id,body){
    return dispatch => {
        return fetch(`${apiLocation()}/update/${id}`,
            putAPI(body)
        )
        .then(response => {
            return response.status
        })
        .then(status => dispatch(client_getAllJobs()))
    }

}

export function updateJob(id,body){
    return dispatch =>{
        return dispatch(client_updateJob(id,body))
    }
}
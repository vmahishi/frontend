import React, {Component} from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import CheckIcon from '@material-ui/icons/Check';
import { Alert, AlertTitle } from '@material-ui/lab';
import './AddJob.css'

export default class AddJob extends Component {

  constructor(props){
    super(props)
    this.state = this.getInitialState();
    this.readInput = this.readInput.bind(this)
    this.addJob = this.addJob.bind(this)
  }

   getInitialState = () => ({
    company : '',
    role : '',
    exp : '',
    education : '',
    location : '',
    industry : '',
    lastDate : '',
    skills : '',
    contact : '',
    responsibility : '',
    open : false
  })

  readInput = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    addJob = () => {
        const body = {
            company : this.state.company,
            role : this.state.role,
            exp : this.state.exp,
            education : this.state.education,
            location :  this.state.location,
            industry : this.state.industry,
            lastDate : this.state.lastDate,
            skills : this.state.skills,
            contact :  this.state.contact,
            responsibility :  this.state.responsibility
        }
        const {dispatch, actions} = this.props
        dispatch(actions.addJob(body))
        this.setState(this.getInitialState());
        this.setState({
            open : true
        })
        setTimeout(() => { 
            this.setState(() => ({open : false}))
        }, 5000);
    }

  render(){
      return (
          <div>
                {
                  this.state.open && 
                  <Snackbar open={this.state.open} autoHideDuration={6000}>
                      <Alert icon={<CheckIcon fontSize="inherit" />} variant="filled" severity="success"> 
                        <AlertTitle>You have successfully added a new job to the system</AlertTitle>
                      </Alert>
                  </Snackbar>
                }
                <form className='form-align' noValidate autoComplete="off">
                    <div>
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Company Name"
                        name="company"
                        multiline
                        rowsMax={4}
                        value={this.state.companyName}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />                    
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Job Role"
                        name="role"
                        multiline
                        rowsMax={4}
                        value={this.state.jobRole}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Experience"
                        name="exp"
                        multiline
                        rowsMax={4}
                        value={this.state.exp}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Education"
                        name="education"
                        multiline
                        rowsMax={4}
                        value={this.state.education}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Location"
                        name="location"
                        multiline
                        rowsMax={4}
                        value={this.state.location}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Industry"
                        name="industry"
                        multiline
                        rowsMax={4}
                        value={this.state.industry}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Last Date for Application"
                        name="lastDate"
                        multiline
                        rowsMax={4}
                        value={this.state.lastDate}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <TextField
                        id="outlined-multiline-flexible"
                        label="Contact Person"
                        name="contact"
                        multiline
                        rowsMax={4}
                        value={this.state.contact}
                        onChange={this.readInput}
                        variant="outlined"
                        className='text-field'
                        />
                        <br/>
                        <TextField
                          id="outlined-full-width"
                          name="skills"
                          label="Primary Skills"
                          style={{ margin: 8 }}
                          placeholder="Placeholder"
                          value={this.state.skills}
                          fullWidth
                          onChange={this.readInput}
                          margin="normal"
                          InputLabelProps={{
                              shrink: true,
                          }}
                          variant="outlined"
                        />
                        <br/>
                        <TextField
                            id="outlined-full-width"
                            name="responsibility"
                            label="Responsibilities"
                            style={{ margin: 8 }}
                            placeholder="Placeholder"
                            value={this.state.responsibility}
                            fullWidth
                            onChange={this.readInput}
                            margin="normal"
                            multiline
                            InputLabelProps={{
                                shrink: true,
                            }}
                            variant="outlined"
                            />
                    </div>
                </form>
                <Button variant="contained" color="primary" onClick={this.addJob}>
                    Add
                </Button>
            </div>
  );
  }
}

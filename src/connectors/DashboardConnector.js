import {connect} from 'react-redux'
import {getAllJobs, deleteJob, addJob, updateJob, getAllApplications} from '../actions/JobAction'
import Scaffolding from '../components/dashboard/Scaffolding'

function mapStateToProps(state){
  return{
    jobList: state.jobList,
    jobApplications: state.jobApplications,
    actions:{
        getAllJobs:getAllJobs,
        addJob: addJob,
        deleteJob:deleteJob,
        updateJob:updateJob,
        getAllApplications:getAllApplications
    }
  } 
}
export default connect(mapStateToProps)(Scaffolding)

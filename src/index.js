import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import configureStore from './configureStore'
import {Provider} from 'react-redux'
import 'whatwg-fetch'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'

const store = configureStore()
const persistor = persistStore(store)
persistor.purge()

ReactDOM.render(
    <Provider store={store}>
     <PersistGate loading={null} persistor={persistor}>
        <App />
    </PersistGate>         
    </Provider>
    , document.getElementById('root'))
registerServiceWorker()

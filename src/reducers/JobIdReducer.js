import {JOB_ID} from '../actions/actionType'

const JobIdReducer = (state=[], action)=> {
    switch(action.type){
        case JOB_ID:
            return action.jobId
        default:
            return state
    }
}

export default JobIdReducer
export const JOB_LIST = 'JOB_LIST'
export const ROLE = 'ROLE'
export const JOB_ID = 'JOB_ID'
export const ERROR = "ERROR"
export const JOB_APPLICATIONS = "JOB_APPLICATIONS"
export const COMPANY_NAME = "COMPANY_NAME"
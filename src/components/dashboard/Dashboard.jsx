import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Chart from './Chart';
import RecentApplicationsOverview from './RecentApplicationsOverview';
import TodaysApplications from './TodaysApplications';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
  },
  fixedHeight: {
    height: 240,
  },
}));

export default function Dashboard() {
    const classes = useStyles();
    const fixedHeightPaper = clsx(classes.paper, classes.fixedHeight);
    return (
        <Grid container spacing={3}>
            <Grid item xs={12} md={8} lg={9}>
            <Paper className={fixedHeightPaper}>
                <Chart />
            </Paper>
            </Grid>
            <Grid item xs={12} md={4} lg={3}>
            <Paper className={fixedHeightPaper}>
                <RecentApplicationsOverview />
            </Paper>
            </Grid>
            <Grid item xs={12}>
            <Paper className={useStyles.paper}>
                <TodaysApplications />
            </Paper>
            </Grid>
        </Grid>
    );
}

import React, { Component } from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import styles from '../home/home.css';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import WireInfo from './EditJob'

export default class Home extends Component {
    constructor(props){
        super(props)
        this.state = {
            showComponent : false,
            id: '',
            showModal : false,
            body : {}
        }
        this.onEdit = this.onEdit.bind(this);
        this.deleteJob = this.deleteJob.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this)
        this.cancelDelete = this.cancelDelete.bind(this)

    }

    componentDidMount = () => {
        const { dispatch, actions } = this.props
        dispatch(actions.getAllJobs())
    }

    deleteJob = (id) => {
        this.setState({
            showComponent : true,
            id : id
        });
    }

    cancelDelete = () => {
        this.setState({
            showComponent : false
            });
    };

    handleClose = () => {
    this.setState({
        showModal : false
    })
  };

    confirmDelete = () => {
        const {actions, dispatch} = this.props;
        dispatch(actions.deleteJob(this.state.id))
        this.setState({
            showComponent : false
        });
    }

    onEdit = (body, jobId) => {
        this.setState({
            showModal : true,
            id : jobId,
            body : body
        })
    }

    render(){
        const jobs = this.props.jobList
            return (
                <div>
                <TableContainer component={Paper}>
                    <Table className={styles.table} aria-label="simple table">
                        <TableHead>
                        <TableRow>
                            <TableCell>Company</TableCell>
                            <TableCell align="left">Role</TableCell>
                            <TableCell align="left">Experience</TableCell>
                            <TableCell align="left">Skills</TableCell>
                            <TableCell align="left">Key Responsibilities</TableCell>
                            <TableCell align="right" size='small'></TableCell>
                            <TableCell align="right" size='small'></TableCell>
                        </TableRow>
                        </TableHead>
                        <TableBody>
                        {jobs && jobs.map((job) => (
                            <TableRow key={job._id}>
                            <TableCell component="th" scope="row">
                                {JSON.parse(job.body).company}
                            </TableCell>
                            <TableCell align="left">{JSON.parse(job.body).role}</TableCell>
                            <TableCell align="left">{JSON.parse(job.body).exp}</TableCell>
                            <TableCell align="left">{JSON.parse(job.body).skills}</TableCell>
                            <TableCell align="left">{JSON.parse(job.body).responsibility}</TableCell>
                            <TableCell align="right" size='small'>
                                <Button variant="contained" color="primary" onClick={() => this.onEdit(JSON.parse(job.body), job._id)}>
                                    Details
                                </Button>
                            </TableCell>
                            <TableCell align="right" size='small'>
                                <Button variant="contained" color="primary" onClick={() => this.deleteJob(job._id)}>
                                    Delete
                                </Button>
                            </TableCell>
                            </TableRow>
                        ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                 <Dialog
                    open={this.state.showComponent}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Are you sure you want to delete"}</DialogTitle>
                    <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                    </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={() => this.cancelDelete()} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={() => this.confirmDelete()} color="primary" autoFocus>
                        Delete
                    </Button>
                    </DialogActions>
                </Dialog>
                {
                    this.state.showModal &&
                    <WireInfo handleClose={this.handleClose} id={this.state.id} body={this.state.body} actions={this.props.actions} dispatch={this.props.dispatch}/>
                }
                </div>
            )
        }
}
import {JOB_APPLICATIONS} from '../actions/actionType'

const ApplicationReducer = (state=[], action)=> {
    switch(action.type){
        case JOB_APPLICATIONS:
            return action.jobApplications
        default:
            return state
    }
}

export default ApplicationReducer
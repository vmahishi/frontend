import React from "react";
import { withStyles } from "@material-ui/core";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import { teal} from "@material-ui/core/colors";

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  primaryColor: {
    color: teal[500]
  },
  padding: {
    padding: 0
  },
  mainHeader: {
    padding: 20,
    alignItems: "center"
  },
  mainContent: {
    padding: 40
  },
  secondaryContainer: {
    padding: "20px 25px"
  }
});

function WireInfo(props) {
  const { classes, handleClose, id, body } = props;
  const [open, setOpen] = React.useState(true);
  const [company, setCompany] = React.useState(body.company)
  const [role, setRole] = React.useState(body.role)
  const [exp, setExp] = React.useState(body.exp)
  const [education, setEducation] = React.useState(body.education)
  const [location, setLocation] = React.useState(body.location)
  const [industry, setIndustry] = React.useState(body.industry)
  const [lastDate, setLastdate] = React.useState(body.lastDate)
  const [skills, setSkills] = React.useState(body.skills)
  const [contact, setContact] = React.useState(body.contact)
  const [responsibility, setResponsibility] = React.useState(body.responsibility)

  const onClose = () => {
    setOpen(false);
    handleClose()
  };

  const Update = () => {
      const body = {
            company : company,
            role : role,
            exp : exp,
            education : education,
            location :  location,
            industry : industry,
            lastDate : lastDate,
            skills : skills,
            contact :  contact,
            responsibility :  responsibility
        }
        const {actions, dispatch} = props
        dispatch(actions.updateJob(id, body))
        setOpen(false);
        handleClose()
  }

  const Cancel = () => {
    setOpen(false);
    handleClose()
  }
  return (
    <Dialog
      className={classes.root}
      fullWidth
      maxWidth="md"
      open={open}
      onClose={onClose}
    >
      <DialogContent className={classes.padding}>
        <Grid container>
          <Grid item xs={8}>
            <Grid container direction="row" className={classes.mainHeader}>
              <Grid item xs={8}>
                <Typography className={classes.primaryColor} variant="h5">
                  Edit Job Details
                </Typography>
              </Grid>
            </Grid>
            <Grid
              container
              direction="row"
              className={classes.mainContent}
              spacing={1}
            >
              <Grid item xs={10} >
                <TextField
                    id="outlined-multiline-flexible"
                    label="Company Name"
                    name="company"
                    multiline
                    rowsMax={4}
                    value={company}
                    onChange={e => setCompany(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={10}>
                <TextField
                    id="outlined-multiline-flexible"
                    label="Job Role"
                    name="role"
                    multiline
                    rowsMax={4}
                    value={role}
                    onChange={e => setRole(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={7}>
                <TextField
                    id="outlined-multiline-flexible"
                    label="Experience"
                    name="exp"
                    multiline
                    rowsMax={4}
                    value={exp}
                    onChange={e => setExp(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={7}>
                <TextField
                    id="outlined-multiline-flexible"
                    label="Education"
                    name="education"
                    multiline
                    rowsMax={4}
                    value={education}
                    onChange={e => setEducation(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    id="outlined-multiline-flexible"
                    label="Location"
                    name="location"
                    multiline
                    rowsMax={4}
                    value={location}
                    onChange={e => setLocation(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    id="outlined-multiline-flexible"
                    label="Industry"
                    name="industry"
                    multiline
                    rowsMax={4}
                    value={industry}
                    onChange={e => setIndustry(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={12}>
                 <TextField
                    id="outlined-multiline-flexible"
                    label="Last Date for Application"
                    name="lastDate"
                    multiline
                    rowsMax={4}
                    value={lastDate}
                    onChange={e => setLastdate(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={12}>
                 <TextField
                    id="outlined-multiline-flexible"
                    label="Contact Person"
                    name="contact"
                    multiline
                    rowsMax={4}
                    value={contact}
                    onChange={e => setContact(e.target.value)}
                    variant="outlined"
                    className='text-field'
                    />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    id="outlined-full-width"
                    name="skills"
                    label="Primary Skills"
                    style={{ margin: 8 }}
                    placeholder="Placeholder"
                    value={skills}
                    fullWidth
                    onChange={e => setSkills(e.target.value)}
                    margin="normal"
                    multiline
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                    id="outlined-full-width"
                    name="responsibility"
                    label="Responsibilities"
                    style={{ margin: 8 }}
                    placeholder="Placeholder"
                    value={responsibility}
                    fullWidth
                    onChange={e => setResponsibility(e.target.value)}
                    margin="normal"
                    multiline
                    InputLabelProps={{
                        shrink: true,
                    }}
                    variant="outlined"
                />
              </Grid>
              <Grid item xs={12}>
                <Button onClick={Update} color="primary" autoFocus>
                    Update
                </Button>
                <Button onClick={Cancel} color="primary" autoFocus>
                    Cancel
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
    </Dialog>
  );
}

export default withStyles(styles)(WireInfo);
